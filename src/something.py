import datetime
print("something will happen now..")

current_time = datetime.datetime.now()
print(f"Now is {current_time} !")

time_diff = datetime.datetime.now() - current_time
print(f"Time passed to execute prints: {time_diff} !")

print("Some new stuff to print")
